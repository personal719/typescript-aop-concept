import hash from 'object-hash'
const cacheObject: any = {};

function _getCacheKey(args: any, methodName: string) {
    const argsAsObject: any = {};
    args.forEach((item: any, index: any) => {
        argsAsObject[`arg_${index}`] = item;
    })
    const hashedKey = hash(argsAsObject);
    return `${methodName}_${hashedKey};`
}

// it just for example we must use redis in the future instead to do cache
export default <T>(cacheKey: string) => {
    return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
        const originalMethod = descriptor.value;
        descriptor.value = function (...args: any[]) {
            return new Promise<T>((resolve) => {
                const cacheKey = _getCacheKey(args, propertyKey);
                if (cacheObject[cacheKey]) {
                    resolve(cacheObject[cacheKey])
                    return;
                } else {
                    originalMethod.apply(this, args).then((res: T) => {
                        cacheObject[cacheKey] = res;
                        resolve(res);
                        return;
                    });
                }

            })
        }
    }
}
