// just for example we could use datadog in the future
export default <T>() => {
    return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
        const originalMethod = descriptor.value;

        descriptor.value = function (...args: any[]) {
            return new Promise<T>((resolve) => {
                const startedDateTime = new Date().getTime();
                originalMethod(...args).then((res: T) => {
                    const endDateTime = new Date().getTime();
                    const methodName = propertyKey;
                    const executionTime = endDateTime - startedDateTime;
                    console.log(`Method [${methodName}] exection time : [${executionTime}] milli seconds`)
                    resolve(res);
                });
            })
        }
    }
}
