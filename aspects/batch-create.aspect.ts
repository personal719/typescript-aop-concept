const groupByReflection: any = {

}
// when items exceed time in memory  it must be call a bulkCreate function and clean an items
const maxTimeToStillItemsInMemory = 1000 * 60;
/*
    * maxItemsSize is maximum of allowed stayed items in memory 
    * reflectionClass is the class which we use to call a bulkCreateFunction
*/
export default <T>(maxItemsSize: number, reflectionClass: any, bulkCreateMethodName: any) => {
    if (!groupByReflection[reflectionClass])
        groupByReflection[reflectionClass] = [];

    return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
        descriptor.value = function (...args: any[]) {
            groupByReflection[reflectionClass].push(args[0])
            if (groupByReflection[reflectionClass].length >= maxItemsSize) {
                return new Promise((resolve) => {
                    // Call bulk create function from reflection class 
                    reflectionClass[bulkCreateMethodName](groupByReflection[reflectionClass])
                    .then((res: T)=>{
                        resolve(res);
                        return;
                    })

                    groupByReflection[reflectionClass] = []; // to clean old items 
                });
            }
        }

    }
}
function applyBulkcCreateItemAndClean(){
    // Todo Clean Items 
}
// setInterval(applyBulkcCreateItemAndClean,maxTimeToStillItemsInMemory);
