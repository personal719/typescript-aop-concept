
import CacheAspect from './aspects/cache.aspect'
import LogginExectiontime from './aspects/logging-execution-time.aspect'
import BatchCreate from './aspects/batch-create.aspect'

class SearchService {
   
    async searchOnSomething(query: string, orderBy: string) {
       await new SearchRepo().create( new SearchLogModel(query));
       return  this.getSearchResult(query,orderBy);
    }

    @LogginExectiontime()
    @CacheAspect<any[]>('search')
    async getSearchResult(query: string,orderBy: string){
        await new Promise(resolve => setTimeout(resolve, 2_000)); // to make function sleep 
            return [
                {
                    title: "Title example 1",
                    link: "https://google.com"
                },
                {
                    title: "Title example 2",
                    link: "https://youtube.com"
                }
            ]
    }
}


class SearchRepo  {
    // when we call a create function 5 times or more bulk create function will be executed
    @BatchCreate(5,SearchRepo,'bulkCreate')
     async create(searchModel: SearchLogModel) {
        await new Promise(resolve => setTimeout(resolve, 3_000)); // to make function sleep 
        console.log("Create function has been finished")
        // TODO: implement sing create function

    }

    static async bulkCreate(models: SearchLogModel[]) {
        await new Promise(resolve => setTimeout(resolve, 3_500)); // to make function sleep 
        console.log("Start bulk create")
        console.log(models)
        console.log("Bulk create function has been finished")

        // TODO: implement bulk create function
    }
}

class SearchLogModel{
    private query: string;
    constructor(query: string){
        this.query = query.toLowerCase();
    }
}
async function main() {
    const searchService = new SearchService();
    await searchService.searchOnSomething('Query#1', 'asc');
    await searchService.searchOnSomething('Query#1', 'asc');
    
    
}
main();


