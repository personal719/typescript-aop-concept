## Brief:
    In some use cases interceptors and middlewares are not enough to apply some of non functional requirment 
    So we need to add some of our cross cut concern  on specific function inside our services not inside our controllers cause those services are used in diffrent classes  for that reason i added those examples.
    Notice: those example to achecive 'AOP(Aspect Oriented Programming)' in diffrent controller level
    

## First case :
### Batch create:
    in some cases we don't need to create record immediately on database so we can instead of that collect those records 
    unit they have specific count and we can do a bulk create in back ground of our application 
    For example 
        - imagine that we have a search applicaiton and we want to store eache query which recieved users in database 
          in this case we can collect queries records and check when we exceed maximum count of record ,do our bulk create function.
example on code :
```
const searchService = new SearchService();
await searchService.searchOnSomething('Query#1', 'asc');
await searchService.searchOnSomething('Query#2', 'asc');
await searchService.searchOnSomething('Query#3', 'asc');
await searchService.searchOnSomething('Query#4', 'asc');
await searchService.searchOnSomething('Query#5', 'asc');
Output will be : 
Start bulk create
[
  SearchLogModel { query: 'query#1' },
  SearchLogModel { query: 'query#2' },
  SearchLogModel { query: 'query#3' },
  SearchLogModel { query: 'query#4' },
  SearchLogModel { query: 'query#5' }
]
```
## Second case:
#### Monitoring performance for some functions.
    in some cases we need to check the execution time of some functions to monitor performance of some of them  regardless of network spent time
     to process request  for watchting the slowest point in our code and that is a crosscut concern so i think we could seperate it from our functions to be in seperated place.
example on code :

```
    @LogginExectiontime()
    async getSearchResult(query: string,orderBy: string){
    
    const searchService = new SearchService();
    await searchService.searchOnSomething('Accountant film', 'asc');
    await searchService.searchOnSomething('Accountant film', 'asc');
    output: will be     
    Method [getSearchResult] exection time : [2008] milli seconds
    Method [getSearchResult] exection time : [2004] milli seconds
```

## Third case :
#### Caching:
    As i have noticed that a lot of function do a caching to enhance performance but that is a crosscut concern not
    a common concern for that reason it should be seperated from our functions which acheived a functional requirment.

example on code :
 ```   
    // add decorators on getSearchResultFunction

    @LogginExectiontime()
    @CacheAspect<any[]>('search')
    async getSearchResult(query: string,orderBy: string){

    const searchService = new SearchService();
    await searchService.searchOnSomething('Accountant film', 'asc');
    await searchService.searchOnSomething('Accountant film', 'asc');
    output: will be     
    Method [getSearchResult] exection time : [2004] milli seconds
    Method [getSearchResult] exection time : [1] milli seconds
```
